# Netbird Dashboard Nixos Flake
This flake runs the Netbird dashboard with Nginx.  The dashboard is a static site, so it just needs to be served with something.

Using the `enableNginx` option is a little misleading.  It will run the dashboard as though Nginx is serving it directly to the public.  This is in contrast to when the option is false.  Then the dashboard will only be served on the configured interface/port configuration combination.

## Basic Configuration

### Flake import:
```nix
{
  description = "Netbird Dashboard Host Flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    netbird-dashboard.url = "git+https://gitlab.com/helpful-nixos-flakes/networking/netbird-dashboard";
    netbird-dashboard.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, ... }@attrs: {
    nixosConfigurations.myhost = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [ ./configuration.nix ];
    };
  };
}

```

### `configuration.nix`:
```nix
{ config, lib, pkgs, netbird-dashboard, ... }:

{
  imports = [ netbird-dashboard.nixosModules.x86_64-linux.default ];

  # minimum configuration that probably wouldn't work for anything
  # serves dashboard to 127.0.0.1:8010
  config.services.netbird-dashboard = {
    enable = true;
    domain = "netbird.mydomain.tld";
    authIssuer = "https://auth.mydomain.tld";
  };
}

```

## TLS
If `enableNginx` is true, then ACME TLS is expected to be configured as the virtual host is `onlySSL = true`.  The idea is that since this is being hosted in public, it should be encrypted.