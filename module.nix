{ config, lib, pkgs, utils, ... }:

let
  inherit (lib) concatStringsSep mapAttrs mkEnableOption mkIf mkOption mkPackageOption types;

  toStringEnv = with lib; value: if isBool value then boolToString value else toString value;

  cfg = config.services.netbird-dashboard;
in {
  options.services.netbird-dashboard = {
    enable = mkEnableOption "Netbird signal service";
    package = mkPackageOption pkgs "netbird-dashboard" { };
    enableNginx = mkEnableOption "Nginx TLS-only reverse-proxy to serve the dashboard.";
    
    domain = mkOption {
      type = types.str;
      example = "netbird.mydomain.tld";
      description = "The domain under which the dashboard runs.";
    };
    
    interface = mkOption {
      type = types.str;
      default = "127.0.0.1";
      example = "netbird.mydomain.tld";
      description = "The interface to bind to.  This is only used when enableNginx is false.";
    };

    port = mkOption {
      type = types.port;
      default = 8010;
      description = ''
        Port that Nginx will bind to for serving the Netbird dashboard.  This option is
        ignored when enableNginx is true since the default Nginx behavior is correct.
      '';
    };

    managementServer = mkOption {
      type = types.str;
      default = "https://${cfg.domain}";
      description = "The address of the Zetbird management server.  Uses the management enpoints.";
    };

    authIssuer = mkOption {
      type = types.str;
      example = "https://auth.auth.tld:1234";
      description = "The auth issuer for the Netbird application.";
    };

    authAudience = mkOption {
      type = types.str;
      default = "netbird";
      description = "The auth audience for the Netbird application.";
    };

    authClientId = mkOption {
      type = types.str;
      default = cfg.authAudience;
      description = "The auth audience for the Netbird application.";
    };

    authSupportedScopes = mkOption {
      type = types.listOf types.str;
      default = [ "openid" "profile" "email" ];
      description = "The auth audience for the Netbird application.";
    };

    authRedirectURI = mkOption {
      type = types.str;
      default = "";
      description = "The auth audience for the Netbird application.";
    };

    authSilentRedirectURI = mkOption {
      type = types.str;
      default = "";
      description = "The auth audience for the Netbird application.";
    };

    useAuth0 = mkOption {
      type = types.bool;
      default = false;
      description = "Needed for Auth0 IDP.";
    };

    finalDrv = mkOption {
      readOnly = true;
      type = types.package;
      description = "The derivation containing the final, templated, static-site dashboard.";
    };
  };

  config = mkIf cfg.enable {
    services.netbird-dashboard = {
      finalDrv = let 
        settings = {
          AUTH_AUDIENCE = cfg.authAudience;
          AUTH_AUTHORITY = cfg.authIssuer;
          AUTH_CLIENT_ID = cfg.authClientId;
          AUTH_CLIENT_SECRET = ""; # not used by anything important
          AUTH_SUPPORTED_SCOPES = concatStringsSep " " cfg.authSupportedScopes;
          AUTH_REDIRECT_URI = cfg.authRedirectURI;
          AUTH_SILENT_REDIRECT_URI = cfg.authSilentRedirectURI;
          NETBIRD_MGMT_API_ENDPOINT = cfg.managementServer;
          NETBIRD_MGMT_GRPC_API_ENDPOINT = cfg.managementServer;
          NETBIRD_TOKEN_SOURCE = "idToken"; # supposed to work better
          USE_AUTH0 = cfg.useAuth0;
        };
      in pkgs.runCommand "netbird-dashboard"
        {
          nativeBuildInputs = [ pkgs.gettext ];
          env = {
            ENV_STR = concatStringsSep " " [
              "$AUTH_AUDIENCE"
              "$AUTH_AUTHORITY"
              "$AUTH_CLIENT_ID"
              "$AUTH_CLIENT_SECRET"
              "$AUTH_REDIRECT_URI"
              "$AUTH_SILENT_REDIRECT_URI"
              "$AUTH_SUPPORTED_SCOPES"
              "$NETBIRD_DRAG_QUERY_PARAMS"
              "$NETBIRD_GOOGLE_ANALYTICS_ID"
              "$NETBIRD_HOTJAR_TRACK_ID"
              "$NETBIRD_MGMT_API_ENDPOINT"
              "$NETBIRD_MGMT_GRPC_API_ENDPOINT"
              "$NETBIRD_TOKEN_SOURCE"
              "$USE_AUTH0"
            ];
          } // (mapAttrs (_: toStringEnv) settings);
        }
        ''
          cp -R ${cfg.package} build

          find build -type d -exec chmod 755 {} \;
          OIDC_TRUSTED_DOMAINS="build/OidcTrustedDomains.js"

          envsubst "$ENV_STR" < "$OIDC_TRUSTED_DOMAINS.tmpl" > "$OIDC_TRUSTED_DOMAINS"

          for f in $(grep -R -l AUTH_SUPPORTED_SCOPES build/); do
            mv "$f" "$f.copy"
            envsubst "$ENV_STR" < "$f.copy" > "$f"
            rm "$f.copy"
          done

          cp -R build $out
        '';
    };

    services.nginx = {
      enable = true;

      virtualHosts.${cfg.domain} = let
        extraConfig = ''
          add_header Cache-Control "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0";
          expires off;
        '';
      in {
        root = cfg.finalDrv;
        listen = mkIf (!cfg.enableNginx) [ { addr = cfg.interface; port = cfg.port;} ];
        onlySSL = cfg.enableNginx;

        locations = {
          "/" = {
            tryFiles = "$uri $uri.html $uri/ =404";
            inherit extraConfig;
          };

          "/404.html" = {
            extraConfig = ''
              internal;
            '' + extraConfig;
          };
        };

        extraConfig = ''
          error_page 404 /404.html;
        '';
      };
    };
  };
}